package startandroid.p004ru.solenoid;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import startandroid.p004ru.solenoid.CommunicatorImpl;

/* renamed from: startandroid.ru.solenoid.MainActivity */
public class MainActivity extends ListActivity {
    public static final String UUID = "00001101-0000-1000-8000-00805F9B34FB";
    private boolean InitAdapter = false;
    public TextView LogBluetooth;
    private BluetoothAdapter bluetoothAdapter;
    /* access modifiers changed from: private */
    public Button btnClearError;
    private Button btnExit;
    private Button btnFors;
    /* access modifiers changed from: private */
    public Button btnID;
    private Button btnLearn;
    /* access modifiers changed from: private */
    public Button btnLearning;
    /* access modifiers changed from: private */
    public Button btnReadError;
    private Button btnSearch;
    /* access modifiers changed from: private */
    public ClientThread clientThread;
    private final CommunicatorService communicatorService = new CommunicatorService() {
        public Communicator createCommunicatorThread(BluetoothSocket socket) {
            return new CommunicatorImpl(socket, new CommunicatorImpl.CommunicationListener() {
                public void onMessage(final String message) {
                    MainActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            MainActivity.this.textData.setText(MainActivity.this.textData.getText().toString() + "\n" + message);
                        }
                    });
                }

                public void onMessage30360700(final String message04) {
                    MainActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            MainActivity.this.textView04.setText(message04);
                            if (message04 == "Заверш.") {
                                MainActivity.this.textView04.setBackgroundColor(Color.parseColor("#61f83c"));
                            }
                        }
                    });
                }

                public void onMessage303601(final String message00) {
                    MainActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            MainActivity.this.textView00.setText(message00);
                        }
                    });
                }

                public void onMessage614C(final String message01, final String message02, final String message03) {
                    MainActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            MainActivity.this.textView01.setText(message01);
                            MainActivity.this.textView02.setText(message02);
                            MainActivity.this.textView03.setText(message03);
                        }
                    });
                }

                public void onMessage61BF(final String message05) {
                    MainActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            MainActivity.this.textviewFors.setText(MainActivity.this.textviewFors.getText().toString() + "\n" + message05);
                        }
                    });
                }
            });
        }
    };
    public String deviceNameID;
    private BroadcastReceiver discoverDevicesReceiver;
    /* access modifiers changed from: private */
    public final List<BluetoothDevice> discoveredDevices = new ArrayList();
    /* access modifiers changed from: private */
    public BroadcastReceiver discoveryFinishedReceiver;
    private LinearLayout learnLayout;
    public LinearLayout linLayout;
    public LinearLayout linlayoutFors;
    /* access modifiers changed from: private */
    public ArrayAdapter<BluetoothDevice> listAdapter;
    public boolean nameid = true;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog;
    private ServerThread serverThread;
    public TextView textData;
    private EditText textMessage;
    public TextView textView00;
    public TextView textView01;
    public TextView textView02;
    public TextView textView03;
    public TextView textView04;
    public TextView textviewFors;

    /* renamed from: startandroid.ru.solenoid.MainActivity$WriteTask */
    private class WriteTask extends AsyncTask<String, Void, Void> {
        private WriteTask() {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(String... args) {
            try {
                MainActivity.this.clientThread.getCommunicator().write(args[0]);
                return null;
            } catch (Exception e) {
                Log.d("MainActivity", e.getClass().getSimpleName() + " " + e.getLocalizedMessage());
                return null;
            }
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(C0349R.layout.main);
        this.learnLayout = (LinearLayout) findViewById(C0349R.C0351id.learnLayout);
        this.LogBluetooth = (TextView) findViewById(C0349R.C0351id.LogBluetooth);
        this.learnLayout.setVisibility(8);
        this.linlayoutFors = (LinearLayout) findViewById(C0349R.C0351id.linlayoutFors);
        this.linlayoutFors.setVisibility(8);
        this.bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (this.bluetoothAdapter != null && this.bluetoothAdapter.getState() == 10) {
            this.bluetoothAdapter.enable();
        }
        this.textData = (TextView) findViewById(C0349R.C0351id.data_text);
        this.textMessage = (EditText) findViewById(C0349R.C0351id.message_text);
        this.textView00 = (TextView) findViewById(C0349R.C0351id.textView00);
        this.textView01 = (TextView) findViewById(C0349R.C0351id.textView01);
        this.textView02 = (TextView) findViewById(C0349R.C0351id.textView02);
        this.textView03 = (TextView) findViewById(C0349R.C0351id.textView03);
        this.textView04 = (TextView) findViewById(C0349R.C0351id.textView04);
        this.textviewFors = (TextView) findViewById(C0349R.C0351id.textviewFors);
        this.btnSearch = (Button) findViewById(C0349R.C0351id.btnSearch);
        this.btnLearning = (Button) findViewById(C0349R.C0351id.btnLearning);
        this.btnLearn = (Button) findViewById(C0349R.C0351id.btnLearn);
        this.btnSearch = (Button) findViewById(C0349R.C0351id.btnSearch);
        this.btnID = (Button) findViewById(C0349R.C0351id.btnID);
        this.btnExit = (Button) findViewById(C0349R.C0351id.btnExit);
        this.btnReadError = (Button) findViewById(C0349R.C0351id.btnReadError);
        this.btnClearError = (Button) findViewById(C0349R.C0351id.btnClearError);
        this.btnLearning.setClickable(false);
        this.btnReadError.setClickable(false);
        this.btnClearError.setClickable(false);
        this.btnID.setClickable(false);
        this.btnLearning.setAlpha(0.5f);
        this.btnReadError.setAlpha(0.5f);
        this.btnClearError.setAlpha(0.5f);
        this.btnID.setAlpha(0.5f);
        this.listAdapter = new ArrayAdapter<BluetoothDevice>(getBaseContext(), 17367043, this.discoveredDevices) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                ((TextView) view.findViewById(16908308)).setText(((BluetoothDevice) getItem(position)).getName());
                return view;
            }
        };
        setListAdapter(this.listAdapter);
    }

    public void makeDiscoverable() {
        Intent i = new Intent("android.bluetooth.adapter.action.REQUEST_DISCOVERABLE");
        i.putExtra("android.bluetooth.adapter.extra.DISCOVERABLE_DURATION", 300);
        startActivity(i);
    }

    public void onBackPressed() {
        new AlertDialog.Builder(this).setTitle("Выйти из приложения?").setMessage("Вы действительно хотите выйти?").setNegativeButton(17039369, (DialogInterface.OnClickListener) null).setPositiveButton(17039379, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                MainActivity.super.onBackPressed();
            }
        }).create().show();
    }

    public void discoverDevices(View view) {
        getListView().setVisibility(0);
        this.discoverDevicesReceiver = null;
        this.discoveredDevices.clear();
        this.listAdapter.notifyDataSetChanged();
        if (this.discoverDevicesReceiver == null) {
            this.discoverDevicesReceiver = new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    if ("android.bluetooth.device.action.FOUND".equals(intent.getAction())) {
                        BluetoothDevice device = (BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                        if (!MainActivity.this.discoveredDevices.contains(device)) {
                            MainActivity.this.discoveredDevices.add(device);
                            MainActivity.this.textData.setText(MainActivity.this.LogBluetooth.getText().toString() + "\n" + "device - \"" + device.getName() + "\"" + "   " + device.getAddress());
                            MainActivity.this.listAdapter.notifyDataSetChanged();
                        }
                    }
                }
            };
        }
        if (this.discoveryFinishedReceiver == null) {
            this.discoveryFinishedReceiver = new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    MainActivity.this.getListView().setEnabled(true);
                    if (MainActivity.this.progressDialog != null) {
                        MainActivity.this.progressDialog.dismiss();
                    }
                    Toast.makeText(MainActivity.this.getBaseContext(), "Поиск закончен. Выберите устройство для обмена.", 0).show();
                    MainActivity.this.unregisterReceiver(MainActivity.this.discoveryFinishedReceiver);
                }
            };
        }
        registerReceiver(this.discoverDevicesReceiver, new IntentFilter("android.bluetooth.device.action.FOUND"));
        registerReceiver(this.discoveryFinishedReceiver, new IntentFilter("android.bluetooth.adapter.action.DISCOVERY_FINISHED"));
        getListView().setEnabled(false);
        this.progressDialog = ProgressDialog.show(this, "Поиск устройств", "Подождите...");
        this.bluetoothAdapter.startDiscovery();
    }

    public void onPause() {
        super.onPause();
        this.bluetoothAdapter.cancelDiscovery();
        if (this.discoverDevicesReceiver != null) {
            try {
                unregisterReceiver(this.discoverDevicesReceiver);
            } catch (Exception e) {
                Log.d("MainActivity", "Не удалось отключить ресивер " + this.discoverDevicesReceiver);
            }
        }
        if (this.clientThread != null) {
            this.clientThread.cancel();
        }
        if (this.serverThread != null) {
            this.serverThread.cancel();
        }
    }

    public void onResume() {
        super.onResume();
        this.serverThread = new ServerThread(this.communicatorService);
        this.serverThread.start();
        this.discoveredDevices.clear();
        this.listAdapter.notifyDataSetChanged();
    }

    public void onListItemClick(ListView parent, View v, int position, long id) {
        if (this.clientThread != null) {
            this.clientThread.cancel();
        }
        this.clientThread = new ClientThread(this.discoveredDevices.get(position), this.communicatorService);
        this.clientThread.start();
        getListView().setVisibility(8);
        this.btnSearch.setClickable(false);
        this.btnSearch.setAlpha(0.8f);
        this.btnSearch.setText("Вы подключились к устройству \"" + this.discoveredDevices.get(position).getName() + "\"");
        new Handler().postDelayed(new Runnable() {
            public void run() {
                MainActivity.this.btnReadError.setClickable(true);
                MainActivity.this.btnReadError.setAlpha(1.0f);
            }
        }, 500);
    }

    public void sendMessage(View view) {
        this.textMessage.setText("1800FF00");
        this.textData.setVisibility(0);
        this.learnLayout.setVisibility(8);
        new Handler().postDelayed(new Runnable() {
            public void run() {
                MainActivity.this.btnLearning.setClickable(true);
                MainActivity.this.btnClearError.setClickable(true);
                MainActivity.this.btnID.setClickable(true);
                MainActivity.this.btnLearning.setAlpha(1.0f);
                MainActivity.this.btnClearError.setAlpha(1.0f);
                MainActivity.this.btnID.setAlpha(1.0f);
            }
        }, 1600);
        if (this.clientThread != null) {
            new WriteTask().execute(new String[]{this.textMessage.getText().toString()});
            this.textData.setText("");
            Toast.makeText(this, "Идет чтение ошибок.", 0).show();
            return;
        }
        Toast.makeText(this, "Сначала выберите устройство.", 0).show();
    }

    public void sendMessage1(View view) {
        this.textMessage.setText("14FF00");
        if (this.clientThread != null) {
            new WriteTask().execute(new String[]{this.textMessage.getText().toString()});
            this.textData.setText("");
            return;
        }
        Toast.makeText(this, "Сначала выберите устройство.", 0).show();
    }

    public void sendMessage0(View view) {
        this.textMessage.setText("IDfors");
        this.btnLearning.setClickable(false);
        this.btnReadError.setClickable(false);
        this.btnClearError.setClickable(false);
        this.btnID.setClickable(false);
        this.btnLearning.setAlpha(0.5f);
        this.btnReadError.setAlpha(0.5f);
        this.btnClearError.setAlpha(0.5f);
        this.btnID.setAlpha(0.5f);
        this.linlayoutFors.setVisibility(0);
        if (this.clientThread != null) {
            new WriteTask().execute(new String[]{this.textMessage.getText().toString()});
            this.textData.setText("");
            Toast.makeText(this, "Вход в проц.чтния ID форсунок.", 0).show();
            return;
        }
        Toast.makeText(this, "Сначала выберите устройство.", 0).show();
    }

    public void sendMessage2(View view) {
        this.textMessage.setText("303601");
        this.btnSearch.setVisibility(8);
        this.textData.setVisibility(8);
        getListView().setVisibility(8);
        this.learnLayout.setVisibility(0);
        this.btnLearning.setClickable(false);
        this.btnReadError.setClickable(false);
        this.btnClearError.setClickable(false);
        this.btnID.setClickable(false);
        this.btnLearning.setAlpha(0.5f);
        this.btnReadError.setAlpha(0.5f);
        this.btnClearError.setAlpha(0.5f);
        this.btnID.setAlpha(0.5f);
        this.btnLearn.setClickable(true);
        this.btnLearn.setAlpha(1.0f);
        if (this.clientThread != null) {
            new WriteTask().execute(new String[]{this.textMessage.getText().toString()});
            this.textData.setText("");
            Toast.makeText(this, "Вход в процедуру инициализации.", 0).show();
            return;
        }
        Toast.makeText(this, "Сначала выберите устройство.", 0).show();
    }

    public void sendMessage3(View view) {
        this.textMessage.setText("30360700");
        this.btnLearning.setClickable(false);
        this.btnReadError.setClickable(false);
        this.btnClearError.setClickable(false);
        this.btnLearn.setClickable(false);
        this.btnLearn.setAlpha(0.5f);
        if (this.clientThread != null) {
            new WriteTask().execute(new String[]{this.textMessage.getText().toString()});
            this.textData.setText("");
            Toast.makeText(this, "Запущена инициализация клапана ТНВД.", 0).show();
            return;
        }
        Toast.makeText(this, "Сначала выберите устройство.", 0).show();
    }

    public void sendMessage4(View view) {
        this.textMessage.setText("Cancel");
        this.btnSearch = (Button) findViewById(C0349R.C0351id.btnSearch);
        this.btnSearch.setVisibility(0);
        this.btnSearch.setClickable(false);
        this.textData.setVisibility(0);
        getListView().setVisibility(8);
        this.learnLayout.setVisibility(8);
        this.linlayoutFors.setVisibility(8);
        this.textData.setText("");
        this.textView00.setText(" - ");
        this.btnLearning.setClickable(false);
        this.btnReadError.setClickable(true);
        this.btnClearError.setClickable(true);
        this.btnReadError.setAlpha(1.0f);
        this.btnClearError.setAlpha(1.0f);
        if (this.clientThread != null) {
            new WriteTask().execute(new String[]{this.textMessage.getText().toString()});
            this.textData.setText("");
            return;
        }
        Toast.makeText(this, "Сначала выберите устройство.", 0).show();
    }

    public void sendMessage5(View view) {
        this.textMessage.setText("21BF");
        this.LogBluetooth.setVisibility(8);
        if (this.clientThread != null) {
            new WriteTask().execute(new String[]{this.textMessage.getText().toString()});
            this.textviewFors.setText("");
            this.textData.setText("");
            return;
        }
        Toast.makeText(this, "Сначала выберите устройство.", 0).show();
    }

    public void sendMessage6(View view) {
        this.textMessage.setText("Exit");
        this.btnReadError.setClickable(true);
        this.btnClearError.setClickable(true);
        this.btnReadError.setAlpha(1.0f);
        this.btnClearError.setAlpha(1.0f);
        this.linlayoutFors.setVisibility(8);
        if (this.clientThread != null) {
            new WriteTask().execute(new String[]{this.textMessage.getText().toString()});
            this.textviewFors.setText("");
            this.textData.setText("");
            return;
        }
        Toast.makeText(this, "Сначала выберите устройство.", 0).show();
    }
}
