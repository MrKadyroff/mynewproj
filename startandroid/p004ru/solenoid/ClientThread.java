package startandroid.p004ru.solenoid;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;
import java.io.IOException;
import java.util.UUID;

/* renamed from: startandroid.ru.solenoid.ClientThread */
public class ClientThread extends Thread {
    public String LogBlue;
    private BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    /* access modifiers changed from: private */
    public volatile Communicator communicator;
    private final CommunicatorService communicatorService;
    BluetoothDevice device;
    private final BluetoothSocket socket;

    public ClientThread(BluetoothDevice device2, CommunicatorService communicatorService2) {
        this.communicatorService = communicatorService2;
        BluetoothSocket tmp = null;
        try {
            tmp = device2.createRfcommSocketToServiceRecord(UUID.fromString(MainActivity.UUID));
        } catch (IOException e) {
            Log.d("ClientThread", e.getLocalizedMessage());
        }
        this.socket = tmp;
    }

    public synchronized Communicator getCommunicator() {
        return this.communicator;
    }

    public void run() {
        this.bluetoothAdapter.cancelDiscovery();
        try {
            this.LogBlue = "ClientThread, About to connect";
            Log.d("ClientThread", "About to connect");
            this.socket.connect();
            Log.d("ClientThread", "Connected");
            synchronized (this) {
                this.communicator = this.communicatorService.createCommunicatorThread(this.socket);
            }
            new Thread(new Runnable() {
                public void run() {
                    Log.d("ClientThread", "Start");
                    ClientThread.this.communicator.startCommunication();
                }
            }).start();
        } catch (IOException e) {
            try {
                this.socket.close();
            } catch (IOException closeException) {
                Log.d("ClientThread", closeException.getLocalizedMessage());
            }
        }
    }

    public void cancel() {
        if (this.communicator != null) {
            this.communicator.stopCommunication();
        }
    }
}
