package startandroid.p004ru.solenoid;

import android.bluetooth.BluetoothSocket;

/* renamed from: startandroid.ru.solenoid.CommunicatorService */
interface CommunicatorService {
    Communicator createCommunicatorThread(BluetoothSocket bluetoothSocket);
}
