package startandroid.p004ru.solenoid;

/* renamed from: startandroid.ru.solenoid.BuildConfig */
public final class BuildConfig {
    public static final String APPLICATION_ID = "startandroid.ru.solenoid";
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "";
    public static final int VERSION_CODE = 4;
    public static final String VERSION_NAME = "1.0.4";
}
