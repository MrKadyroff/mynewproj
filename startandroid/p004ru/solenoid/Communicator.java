package startandroid.p004ru.solenoid;

/* renamed from: startandroid.ru.solenoid.Communicator */
interface Communicator {
    void startCommunication();

    void stopCommunication();

    void write(String str);
}
