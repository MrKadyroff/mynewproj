package startandroid.p004ru.solenoid;

import android.bluetooth.BluetoothSocket;
import android.os.Environment;
import android.support.p000v4.view.InputDeviceCompat;
import android.util.Log;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/* renamed from: startandroid.ru.solenoid.CommunicatorImpl */
public class CommunicatorImpl extends Thread implements Communicator {
    public boolean CancelLearning = true;
    public boolean ClearError = true;
    public String[] Cmd14FF00 = {"ATSH7E0", "1092", "14FF00"};
    public String[] Cmd21BF = {"ATS0", "ATSH7E0", "1092", "21BF", "ATS1"};
    public String[] Cmd303601 = {"ATSH7E0", "1092", "3E01", "303601", "214C"};
    public String[] CmdElmInit = {"ATZ", "ATE1", "ATSP6", "ATD0", "ATSH7E0", "ATE0", "1092", "3E01", "1800FF00"};
    final String DIR_SD = "TNVD_Logs";
    final String FILENAME_SD = "TNVD_log.txt";
    public int InProgress = 0;
    final String LOG_TAG = "myLogs";
    public boolean Learn = true;
    public boolean Learning = true;
    public boolean ReadError = true;
    public boolean ReadFors = true;
    public int bytes;
    public String deviceNameID = "";
    public File file;
    private final InputStream inputStream;
    private final CommunicationListener listener;
    public int nCmd14FF00 = 0;
    public int nCmd21BF = 0;
    public int nCmd303601 = 0;
    public int nCmdElmInit = 0;
    public boolean nameid = true;
    private final OutputStream outputStream;
    public boolean readAnswer = true;
    public int select02 = 0;
    private final BluetoothSocket socket;
    public long time = System.currentTimeMillis();
    public boolean waitAnswer = true;

    /* renamed from: startandroid.ru.solenoid.CommunicatorImpl$CommunicationListener */
    interface CommunicationListener {
        void onMessage(String str);

        void onMessage303601(String str);

        void onMessage30360700(String str);

        void onMessage614C(String str, String str2, String str3);

        void onMessage61BF(String str);
    }

    public CommunicatorImpl(BluetoothSocket socket2, CommunicationListener listener2) {
        this.socket = socket2;
        this.listener = listener2;
        InputStream tmpIn = null;
        OutputStream tmpOut = null;
        try {
            tmpIn = socket2.getInputStream();
            tmpOut = socket2.getOutputStream();
        } catch (IOException e) {
            Log.d("CommunicatorImpl", e.getLocalizedMessage());
        }
        this.inputStream = tmpIn;
        this.outputStream = tmpOut;
    }

    public void startCommunication() {
        byte[] buffer = new byte[1024];
        byte[] ReadData = new byte[1024];
        byte[] DataInterim = new byte[1024];
        byte[] DataInterimBS = new byte[1024];
        byte[] bArr = new byte[1024];
        byte[] CountError = new byte[4];
        this.bytes = 0;
        if (!Environment.getExternalStorageState().equals("mounted")) {
            Log.d("myLogs", "SD-карта не доступна: " + Environment.getExternalStorageState());
            return;
        }
        File sdPath = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + "TNVD_Logs");
        sdPath.mkdirs();
        File file2 = new File(sdPath, "TNVD_log.txt");
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(file2));
            while (true) {
                try {
                    if (!this.waitAnswer) {
                        buffer[this.bytes] = (byte) this.inputStream.read();
                        if (this.nameid) {
                            bw.append("Open log Init_TNVD:\n");
                            this.nameid = false;
                        }
                        if (buffer[this.bytes] == 62) {
                            if (buffer[0] != 63) {
                                bw.append(" " + (System.currentTimeMillis() - this.time) + ":  " + new String(buffer).substring(0, this.bytes) + 10);
                            }
                            if (!this.ReadError) {
                                this.nCmdElmInit++;
                                if (this.nCmdElmInit < this.CmdElmInit.length) {
                                    write("1800FF00");
                                    this.readAnswer = true;
                                }
                                if (this.nCmdElmInit == 9) {
                                    this.nCmdElmInit = 4;
                                }
                            }
                            if (!this.ClearError) {
                                this.nCmd14FF00++;
                                if (this.nCmd14FF00 < this.Cmd14FF00.length) {
                                    write("14FF00");
                                    this.readAnswer = true;
                                } else {
                                    this.nCmd14FF00 = 0;
                                }
                            }
                            if (!this.ReadFors) {
                                this.nCmd21BF++;
                                if (this.nCmd21BF < this.Cmd21BF.length) {
                                    write("21BF");
                                    this.readAnswer = true;
                                } else {
                                    this.nCmd21BF = 0;
                                }
                            }
                            if (!this.Learning && this.CancelLearning) {
                                this.nCmd303601++;
                                this.readAnswer = true;
                                if (this.nCmd303601 < this.Cmd303601.length) {
                                    write("303601");
                                    this.readAnswer = true;
                                } else {
                                    this.nCmd303601 = 3;
                                    write("303601");
                                    this.readAnswer = true;
                                }
                            }
                            if ((buffer[0] <= 57) && (buffer[0] >= 49)) {
                                ASChexArray(buffer, ReadData, this.bytes);
                            } else if (buffer[0] == 48) {
                                for (int i = 0; i < this.bytes; i++) {
                                    DataInterim[i] = buffer[i + 8];
                                    DataInterimBS[i] = buffer[i + 8];
                                }
                                DeleteSpase(DataInterimBS);
                                MultiStrOneStrResponse(DataInterim);
                                ASChexArray(DataInterim, ReadData, this.bytes);
                            }
                            if (buffer[0] == 78 && buffer[1] == 79) {
                                this.listener.onMessage("Нет ответа от эбу.\n");
                            }
                            if (ReadData[0] == 84) {
                                this.listener.onMessage("Удаление ошибок выполнено.");
                                this.listener.onMessage("Проведите повторное чтение ошибок.\n");
                            }
                            if (ReadData[0] == 112) {
                                if (ReadData[1] == 54 && ReadData[2] == 1) {
                                    if ((ReadData[3] & 255) == 255) {
                                        this.listener.onMessage303601("иниц");
                                        if (this.InProgress != 0 && this.select02 == 2) {
                                            this.listener.onMessage30360700("Заверш.");
                                        }
                                    } else {
                                        this.listener.onMessage303601("не иниц");
                                    }
                                }
                                if (ReadData[1] == 54 && ReadData[2] == 7) {
                                    this.Cmd303601[3] = "303601";
                                    this.InProgress++;
                                }
                            }
                            if (ReadData[0] == 97 && ReadData[1] == 76) {
                                this.listener.onMessage614C(Integer.toString(ReadData[6] & 255), Integer.toString(((ReadData[5] & 255) * 10) - 1280), Integer.toString((ReadData[4] & 255) * 25));
                                if ((ReadData[6] & 255) == 2) {
                                    this.select02 = 2;
                                }
                            }
                            if ((ReadData[0] & 255) == 191) {
                                this.listener.onMessage61BF("1st ID: " + new String(DataInterimBS).substring(2, 32) + 10);
                                this.listener.onMessage61BF("2nd ID: " + new String(DataInterimBS).substring(32, 62) + 10);
                                this.listener.onMessage61BF("3rd ID: " + new String(DataInterimBS).substring(62, 92) + 10);
                                this.listener.onMessage61BF("4th ID: " + new String(DataInterimBS).substring(92, 122) + 10);
                            }
                            if (!(ReadData[0] == Byte.MAX_VALUE && (ReadData[1] & 255) == 48 && (ReadData[2] & 255) != 34)) {
                            }
                            if (ReadData[0] == 88) {
                                this.nCmdElmInit = 4;
                                CountError[0] = ReadData[1];
                                if ((CountError[0] <= 15) && (CountError[0] >= 10)) {
                                    this.listener.onMessage("Найдено ошибок по двигателю:   " + Integer.toString(CountError[0]) + 10);
                                }
                                if ((CountError[0] <= 9) && (CountError[0] >= 0)) {
                                    this.listener.onMessage("Найдено ошибок по двигателю:   " + Integer.toHexString(CountError[0]) + 10);
                                }
                                for (int i2 = 2; i2 < ((CountError[0] & 15) * 3) + 1; i2 = i2 + 1 + 1 + 1) {
                                    int c = ((ReadData[i2] & 255) << 8) | (ReadData[i2 + 1] & 255);
                                    switch (c) {
                                        case 22:
                                            this.listener.onMessage("P0016 -      Crank angle sensor/camshaft position sensor phase problem.\n");
                                            break;
                                        case 71:
                                            this.listener.onMessage("P0047 -      Variable geometry control solenoid valve circuit low input.\n");
                                            break;
                                        case 72:
                                            this.listener.onMessage("P0048 -      Variable geometry control solenoid valve circuit high input.\n");
                                            break;
                                        case 114:
                                            this.listener.onMessage("P0072 -      No.2 intake air temperature sensor circuit low input.\n");
                                            break;
                                        case 115:
                                            this.listener.onMessage("P0073 -      No.2 intake air temperature sensor circuit high input.\n");
                                            break;
                                        case 136:
                                            this.listener.onMessage("P0088 -      Common rail high pressure malfunction.\n");
                                            break;
                                        case 137:
                                            this.listener.onMessage("P0089 -      Suction control valve stuck.\n");
                                            break;
                                        case 147:
                                            this.listener.onMessage("P0093 -      Fuel leak problem.\n");
                                            break;
                                        case 258:
                                            this.listener.onMessage("P0102 -      Air flow sensor circuit low input.\n");
                                            break;
                                        case 259:
                                            this.listener.onMessage("P0103 -      Air flow sensor circuit high input.\n");
                                            break;
                                        case 262:
                                            this.listener.onMessage("P0106 -      Manifold absolute pressure sensor range/performance problem.\n");
                                            break;
                                        case 263:
                                            this.listener.onMessage("P0107 -      Manifold absolute pressure sensor circuit low input.\n");
                                            break;
                                        case 264:
                                            this.listener.onMessage("P0108 -      Manifold absolute pressure sensor circuit high input.\n");
                                            break;
                                        case 274:
                                            this.listener.onMessage("P0112 -      No.1 intake air temperature sensor circuit low input.\n");
                                            break;
                                        case 275:
                                            this.listener.onMessage("P0113 -      No.1 intake air temperature sensor circuit high input.\n");
                                            break;
                                        case 279:
                                            this.listener.onMessage("P0117 -      Engine coolant temperature sensor circuit low input.\n");
                                            break;
                                        case 280:
                                            this.listener.onMessage("P0118 -      Engine coolant temperature sensor circuit high input.\n");
                                            break;
                                        case 290:
                                            this.listener.onMessage("P0122 -      Throttle position sensor circuit low input.\n");
                                            break;
                                        case 291:
                                            this.listener.onMessage("P0123 -      Throttle position sensor circuit high input.\n");
                                            break;
                                        case 386:
                                            this.listener.onMessage("P0182 -      Fuel temperature sensor circuit low input.\n");
                                            break;
                                        case 387:
                                            this.listener.onMessage("P0183 -      Fuel temperature sensor circuit high input.\n");
                                            break;
                                        case 401:
                                            this.listener.onMessage("P0191 -      Rail pressure sensor range/performance problem.\n");
                                            break;
                                        case 402:
                                            this.listener.onMessage("P0192 -      Rail pressure sensor circuit low input.\n");
                                            break;
                                        case 403:
                                            this.listener.onMessage("P0193 -      Rail pressure sensor circuit high input.\n");
                                            break;
                                        case InputDeviceCompat.SOURCE_DPAD:
                                            this.listener.onMessage("P0201 -      No.1 injector circuit malfunction.\n");
                                            break;
                                        case 514:
                                            this.listener.onMessage("P0202 -      No.2 injector circuit malfunction.\n");
                                            break;
                                        case 515:
                                            this.listener.onMessage("P0203 -      No.3 injector circuit malfunction.\n");
                                            break;
                                        case 516:
                                            this.listener.onMessage("P0204 -      No.4 injector circuit malfunction.\n");
                                            break;
                                        case 537:
                                            this.listener.onMessage("P0219 -      Engine overspeed condition.\n");
                                            break;
                                        case 564:
                                            this.listener.onMessage("P0234 -      Turbocharger overboost condition.\n");
                                            break;
                                        case 665:
                                            this.listener.onMessage("P0299 -      Turbocharger under boost condition.\n");
                                            break;
                                        case 769:
                                            this.listener.onMessage("P0301 -      No.1 cylinder injector malfunction.\n");
                                            break;
                                        case 770:
                                            this.listener.onMessage("P0302 -      No.2 cylinder injector malfunction.\n");
                                            break;
                                        case 771:
                                            this.listener.onMessage("P0303 -      No.3 cylinder injector malfunction.\n");
                                            break;
                                        case 772:
                                            this.listener.onMessage("P0304 -      No.4 cylinder injector malfunction.\n");
                                            break;
                                        case 821:
                                            this.listener.onMessage("P0335 -      Crank angle sensor system.\n");
                                            break;
                                        case 822:
                                            this.listener.onMessage("P0336 -      Crank angle sensor range/performance problem.\n");
                                            break;
                                        case 832:
                                            this.listener.onMessage("P0340 -      Camshaft position sensor system.\n");
                                            break;
                                        case 833:
                                            this.listener.onMessage("P0341 -      Camshaft position sensor range/performance problem.\n");
                                            break;
                                        case 1027:
                                            this.listener.onMessage("P0403 -      Exhaust gas recirculation valve (DC motor) malfunction.\n");
                                            break;
                                        case 1029:
                                            this.listener.onMessage("P0405 -      Exhaust gas recirculation valve position sensor circuit low input.\n");
                                            break;
                                        case 1030:
                                            this.listener.onMessage("P0406 -      Exhaust gas recirculation valve position sensor circuit high input.\n");
                                            break;
                                        case 1282:
                                            this.listener.onMessage("P0502 -      Vehicle speed sensor low input (2WD,4WD).\n");
                                            break;
                                        case 1299:
                                            this.listener.onMessage("P0513 -      Immobilizer malfunction (Vehicles with immobilizer system).\n");
                                            break;
                                        case 1361:
                                            this.listener.onMessage("P0551 -      Power steering fluid pressure switch system.\n");
                                            break;
                                        case 1539:
                                            this.listener.onMessage("P0603 -      EEPROM malfunction.\n");
                                            break;
                                        case 1540:
                                            this.listener.onMessage("P0604 -      Random access memory (RAM) malfunction.\n");
                                            break;
                                        case 1541:
                                            this.listener.onMessage("P0605 -      Read only memory (FLASH ROM) malfunction.\n");
                                            break;
                                        case 1542:
                                            this.listener.onMessage("P0606 -      Engine-ECU (main CPU) malfunction.\n");
                                            break;
                                        case 1543:
                                            this.listener.onMessage("P0607 -      Engine-ECU (sub CPU) malfunction.\n");
                                            break;
                                        case 1576:
                                            this.listener.onMessage("P0628 -      Suction control valve open.\n");
                                            break;
                                        case 1577:
                                            this.listener.onMessage("P0629 -      Suction control valve battery short.\n");
                                            break;
                                        case 1584:
                                            this.listener.onMessage("P0630 -      Chassis number not programmed (Vehicles with immobilizer system).\n");
                                            break;
                                        case 1592:
                                            this.listener.onMessage("P0638 -      Throttle valve control servo stuck.\n");
                                            break;
                                        case 1602:
                                            this.listener.onMessage("P0642 -      Analog sensor reference voltage No.1 too low.\n");
                                            break;
                                        case 1603:
                                            this.listener.onMessage("P0643 -      Analog sensor reference voltage No.1 too high.\n");
                                            break;
                                        case 1618:
                                            this.listener.onMessage("P0652 -      Analog sensor reference voltage No.2 too low.\n");
                                            break;
                                        case 1619:
                                            this.listener.onMessage("P0653 -      Analog sensor reference voltage No.2 too high.\n");
                                            break;
                                        case 4611:
                                            this.listener.onMessage("P1203 -      Capacitor insufficient charging.\n");
                                            break;
                                        case 4612:
                                            this.listener.onMessage("P1204 -      Capacitor excessive charging.\n");
                                            break;
                                        case 4722:
                                            this.listener.onMessage("P1272 -      Pressure limiter malfunction.\n");
                                            break;
                                        case 4723:
                                            this.listener.onMessage("P1273 -      Supply pump insufficient flow.\n");
                                            break;
                                        case 4724:
                                            this.listener.onMessage("P1274 -      Supply pump protection.\n");
                                            break;
                                        case 4725:
                                            this.listener.onMessage("P1275 -      Supply pump exchange.\n");
                                            break;
                                        case 4726:
                                            this.listener.onMessage("P1276 -      Fuel filter exchange.\n");
                                            break;
                                        case 4727:
                                            this.listener.onMessage("P1277 -      Fuel filter freeze.\n");
                                            break;
                                        case 4760:
                                            this.listener.onMessage("P1298 -      Variable geometry turbocharger control system malfunction (high pressure).\n");
                                            break;
                                        case 4761:
                                            this.listener.onMessage("P1299 -      Variable geometry turbocharger control system malfunction (low pressure).\n");
                                            break;
                                        case 5669:
                                            this.listener.onMessage("P1625 -      Injection quantity compensation value error.\n");
                                            break;
                                        case 5670:
                                            this.listener.onMessage("P1626 -      Injection quantity compensation value not coding.\n");
                                            break;
                                        case 8472:
                                            this.listener.onMessage("P2118 -      Throttle valve control (DC motor) current malfunction.\n");
                                            break;
                                        case 8482:
                                            this.listener.onMessage("P2122 -      Accelerator pedal position sensor (main) circuit low input.\n");
                                            break;
                                        case 8483:
                                            this.listener.onMessage("P2123 -      Accelerator pedal position sensor (main) circuit high input.\n");
                                            break;
                                        case 8484:
                                            this.listener.onMessage("P2124 -      Accelerator pedal position sensor (main) circuit high input intermittent.\n");
                                            break;
                                        case 8487:
                                            this.listener.onMessage("P2127 -      Accelerator pedal position sensor (sub) circuit low input.\n");
                                            break;
                                        case 8488:
                                            this.listener.onMessage("P2128 -      Accelerator pedal position sensor (sub) circuit high input.\n");
                                            break;
                                        case 8504:
                                            this.listener.onMessage("P2138 -      Accelerator pedal position sensor (main and sub) range/performance problem.\n");
                                            break;
                                        case 8518:
                                            this.listener.onMessage("P2146 -      Injector common 1 (cylinder No.1 and No.4) circuit open.\n");
                                            break;
                                        case 8519:
                                            this.listener.onMessage("P2147 -      Injector common circuit earth short.\n");
                                            break;
                                        case 8520:
                                            this.listener.onMessage("P2148 -      Injector common circuit battery short.\n");
                                            break;
                                        case 8521:
                                            this.listener.onMessage("P2149 -      Injector common 2 (cylinder No.2 and No.3) circuit open.\n");
                                            break;
                                        case 8744:
                                            this.listener.onMessage("P2228 -      Barometric pressure sensor circuit low input.\n");
                                            break;
                                        case 8745:
                                            this.listener.onMessage("P2229 -      Barometric pressure sensor circuit high input.\n");
                                            break;
                                        case 9235:
                                            this.listener.onMessage("P2413 -      Exhaust gas recirculation system performance.\n");
                                            break;
                                        case 53363:
                                            this.listener.onMessage("U1073 -      Bus off.\n");
                                            break;
                                        case 53505:
                                            this.listener.onMessage("U1101 -      A/T-ECU time-out.\n");
                                            break;
                                        case 53506:
                                            this.listener.onMessage("U1102 -      ASTC-ECU Time out.\n");
                                            break;
                                        case 53513:
                                            this.listener.onMessage("U1109 -      ETACS-ECU time-out.\n");
                                            break;
                                        case 53527:
                                            this.listener.onMessage("U1117 -      ETACS-ECU (Immobilizer-ECU) time-out.\n");
                                            break;
                                        default:
                                            String s = Integer.toHexString(c);
                                            this.listener.onMessage("P0" + s.substring(0, s.length() + 0) + " -      Нет описания кода ошибки." + 10);
                                            break;
                                    }
                                }
                            }
                            this.bytes = 0;
                            for (int i3 = 0; i3 < 100; i3++) {
                                ReadData[i3] = 0;
                            }
                        } else {
                            this.bytes++;
                        }
                    }
                } catch (IOException e) {
                    Log.e("", "disconnected", e);
                    bw.append("Close.");
                    bw.close();
                    Log.d("myLogs", "Файл записан на SD: " + file2.getAbsolutePath());
                    return;
                }
            }
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    public void write(String message) {
        if (message.trim().equals("14FF00")) {
            this.Learning = true;
            this.Learn = true;
            this.ReadError = true;
            this.CancelLearning = true;
            this.ClearError = false;
            try {
                Log.d("CommunicatorImpl", "Write " + message);
                this.waitAnswer = false;
                this.outputStream.write((this.Cmd14FF00[this.nCmd14FF00] + "\r").getBytes());
            } catch (IOException e) {
                Log.d("CommunicatorImpl", e.getLocalizedMessage());
            }
            this.readAnswer = true;
        }
        if (message.trim().equals("1800FF00")) {
            this.Learning = true;
            this.Learn = true;
            this.CancelLearning = true;
            this.ClearError = true;
            this.ReadError = false;
            try {
                Log.d("CommunicatorImpl", "Write " + message);
                this.outputStream.write((this.CmdElmInit[this.nCmdElmInit] + 13).getBytes());
                this.waitAnswer = false;
            } catch (IOException e2) {
                Log.d("CommunicatorImpl", e2.getLocalizedMessage());
            }
            this.readAnswer = true;
        }
        if (message.trim().equals("303601")) {
            this.CancelLearning = true;
            this.ReadError = true;
            this.ClearError = true;
            this.Learn = true;
            this.Learning = false;
            try {
                Log.d("CommunicatorImpl", "Write " + message);
                this.waitAnswer = false;
                this.outputStream.write((this.Cmd303601[this.nCmd303601] + 13).getBytes());
            } catch (IOException e3) {
                Log.d("CommunicatorImpl", e3.getLocalizedMessage());
            }
            this.readAnswer = true;
        }
        if (message.trim().equals("IDfors")) {
            this.CancelLearning = true;
            this.ReadError = true;
        }
        if (message.trim().equals("30360700")) {
            this.CancelLearning = true;
            this.ReadError = true;
            do {
            } while (!this.readAnswer);
            this.Cmd303601[3] = "30360700";
        }
        if (message.trim().equals("Cancel")) {
            this.ReadError = true;
            this.Learning = true;
            this.Learn = true;
            this.CancelLearning = false;
            this.InProgress = 0;
            this.listener.onMessage30360700(" - ");
            this.Cmd303601[3] = "303601";
            this.nCmdElmInit = 4;
        }
        if (message.trim().equals("21BF")) {
            this.ReadFors = false;
            try {
                Log.d("CommunicatorImpl", "Write " + message);
                this.waitAnswer = false;
                this.outputStream.write((this.Cmd21BF[this.nCmd21BF] + "\r").getBytes());
            } catch (IOException e4) {
                Log.d("CommunicatorImpl", e4.getLocalizedMessage());
            }
        }
        if (message.trim().equals("Exit")) {
            this.ReadFors = true;
            this.nCmd21BF = 0;
        }
    }

    public void stopCommunication() {
        try {
            this.socket.close();
        } catch (IOException e) {
            Log.d("CommunicatorImpl", e.getLocalizedMessage());
        }
    }

    public void ASChexArray(byte[] inbuf, byte[] outbuf, int bytesread) {
        int k = 0;
        for (int x = 0; x < bytesread - 1; x = x + 2 + 1) {
            byte a = inbuf[x];
            byte b = inbuf[x + 1];
            if (a >= 48 && a <= 57) {
                a = (byte) (a & 15);
            }
            if (b >= 48 && b <= 57) {
                b = (byte) (b & 15);
            }
            for (byte i = 65; i <= 70; i = (byte) (i + 1)) {
                if (a == i) {
                    a = (byte) (a - 55);
                }
                if (b == i) {
                    b = (byte) (b - 55);
                }
            }
            outbuf[k] = (byte) (((byte) (a << 4)) + ((byte) (b & 15)));
            k++;
        }
    }

    public void MultiStrOneStrResponse(byte[] inbuf) {
        for (int i = 0; i < 4; i++) {
            for (int j = 18; j < 255; j++) {
                inbuf[j] = inbuf[j + 1];
            }
        }
        for (int i2 = 0; i2 < 4; i2++) {
            for (int j2 = 39; j2 < 255; j2++) {
                inbuf[j2] = inbuf[j2 + 1];
            }
        }
        for (int i3 = 0; i3 < 4; i3++) {
            for (int j3 = 60; j3 < 255; j3++) {
                inbuf[j3] = inbuf[j3 + 1];
            }
        }
        for (int i4 = 0; i4 < 4; i4++) {
            for (int j4 = 81; j4 < 255; j4++) {
                inbuf[j4] = inbuf[j4 + 1];
            }
        }
        for (int i5 = 0; i5 < 4; i5++) {
            for (int j5 = 102; j5 < 255; j5++) {
                inbuf[j5] = inbuf[j5 + 1];
            }
        }
        for (int i6 = 0; i6 < 4; i6++) {
            for (int j6 = 123; j6 < 255; j6++) {
                inbuf[j6] = inbuf[j6 + 1];
            }
        }
        for (int i7 = 0; i7 < 4; i7++) {
            for (int j7 = 144; j7 < 255; j7++) {
                inbuf[j7] = inbuf[j7 + 1];
            }
        }
        for (int i8 = 0; i8 < 4; i8++) {
            for (int j8 = 165; j8 < 255; j8++) {
                inbuf[j8] = inbuf[j8 + 1];
            }
        }
    }

    public void DeleteSpase(byte[] inbuf) {
        for (int i = 0; i < 3; i++) {
            for (int j = 10; j < 255; j++) {
                inbuf[j] = inbuf[j + 1];
            }
        }
        for (int i2 = 0; i2 < 3; i2++) {
            for (int j2 = 24; j2 < 255; j2++) {
                inbuf[j2] = inbuf[j2 + 1];
            }
        }
        for (int i3 = 0; i3 < 3; i3++) {
            for (int j3 = 38; j3 < 255; j3++) {
                inbuf[j3] = inbuf[j3 + 1];
            }
        }
        for (int i4 = 0; i4 < 3; i4++) {
            for (int j4 = 52; j4 < 255; j4++) {
                inbuf[j4] = inbuf[j4 + 1];
            }
        }
        for (int i5 = 0; i5 < 3; i5++) {
            for (int j5 = 66; j5 < 255; j5++) {
                inbuf[j5] = inbuf[j5 + 1];
            }
        }
        for (int i6 = 0; i6 < 3; i6++) {
            for (int j6 = 80; j6 < 255; j6++) {
                inbuf[j6] = inbuf[j6 + 1];
            }
        }
        for (int i7 = 0; i7 < 3; i7++) {
            for (int j7 = 94; j7 < 255; j7++) {
                inbuf[j7] = inbuf[j7 + 1];
            }
        }
        for (int i8 = 0; i8 < 3; i8++) {
            for (int j8 = 108; j8 < 255; j8++) {
                inbuf[j8] = inbuf[j8 + 1];
            }
        }
    }
}
