package startandroid.p004ru.solenoid;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.util.Log;
import java.io.IOException;
import java.util.UUID;

/* renamed from: startandroid.ru.solenoid.ServerThread */
public class ServerThread extends Thread {
    private final BluetoothServerSocket bluetoothServerSocket;
    private final CommunicatorService communicatorService;

    public ServerThread(CommunicatorService communicatorService2) {
        this.communicatorService = communicatorService2;
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter != null && bluetoothAdapter.getState() == 10) {
            bluetoothAdapter.enable();
        }
        BluetoothServerSocket tmp = null;
        try {
            tmp = bluetoothAdapter.listenUsingRfcommWithServiceRecord("BluetoothApp", UUID.fromString(MainActivity.UUID));
        } catch (IOException e) {
            Log.d("ServerThread", e.getLocalizedMessage());
        }
        this.bluetoothServerSocket = tmp;
    }

    public void run() {
        BluetoothSocket socket = null;
        Log.d("ServerThread", "Started");
        loop0:
        while (true) {
            try {
                BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                if (bluetoothAdapter != null && bluetoothAdapter.getState() == 12) {
                    long endTime = System.currentTimeMillis() + 300;
                    while (System.currentTimeMillis() < endTime) {
                        synchronized (this) {
                            try {
                                wait(endTime - System.currentTimeMillis());
                                try {
                                    socket = this.bluetoothServerSocket.accept();
                                } catch (Exception e) {
                                }
                            } catch (Exception e2) {
                            }
                        }
                    }
                }
                if (socket != null) {
                    this.communicatorService.createCommunicatorThread(socket).startCommunication();
                }
            } catch (Exception e3) {
                Log.d("ServerThread", "Stop: " + e3.getLocalizedMessage());
                return;
            }
        }
    }

    public void cancel() {
        try {
            long endTime = System.currentTimeMillis() + 200;
            while (System.currentTimeMillis() < endTime) {
                synchronized (this) {
                    try {
                        wait(endTime - System.currentTimeMillis());
                        try {
                            this.bluetoothServerSocket.close();
                        } catch (Exception e) {
                        }
                    } catch (Exception e2) {
                    }
                }
            }
        } catch (Exception e3) {
            Log.d("ServerThread", e3.getLocalizedMessage());
        }
    }
}
