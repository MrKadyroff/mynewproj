package android.support.p000v4.app;

/* renamed from: android.support.v4.app.BaseFragmentActivityEclair */
abstract class BaseFragmentActivityEclair extends BaseFragmentActivityDonut {
    BaseFragmentActivityEclair() {
    }

    /* access modifiers changed from: package-private */
    public void onBackPressedNotHandled() {
        super.onBackPressed();
    }
}
